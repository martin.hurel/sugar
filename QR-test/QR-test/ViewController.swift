//
//  ViewController.swift
//  QR-test
//
//  Created by Clémence on 09/12/2019.
//  Copyright © 2019 Clémence. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var video = AVCaptureVideoPreviewLayer()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Creer la session
        let session = AVCaptureSession()
        
        let captureDevice = AVCaptureDevice.default(for: .video)
        
        do
        {
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            session.addInput(input)
        }
        catch{
            print("ERROR")
        }
        
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        
        session.startRunning()
        
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects != nil && metadataObjects.count != nil
        {
            if var object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
            {
                if object.type == AVMetadataObject.ObjectType.qr
                {
//                    print(object.stringValue)
//                    let alert = UIAlertController(title: "Scan détecté", message: nil, preferredStyle: .alert)
//
//                    alert.addAction(UIAlertAction(title: "Voir les sucres", style: .default, handler: nil))
//                    alert.addAction(UIAlertAction(title: "Nouveau scan", style: .default, handler: nil))
//                    self.present(alert, animated: true)
                    
                    
                    
                }
            }
        }
        
    }

}

