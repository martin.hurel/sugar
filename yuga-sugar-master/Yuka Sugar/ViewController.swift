//
//  ViewController.swift
//  Yuka Sugar
//
//  Created by Quentin LUBACK on 04/12/2019.
//  Copyright © 2019 Quentin LUBACK. All rights reserved.
//

import UIKit
import SceneKit
import ARKit
import AVFoundation

class ViewController: UIViewController,AVCaptureMetadataOutputObjectsDelegate, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    var video = AVCaptureVideoPreviewLayer()
    var ar = false
    var qr = true
    
    
    override func viewDidLoad() {
        
//super.viewDidLoad()
        
        
//        if ar == true
//        {
//            super.viewDidLoad()
//            print("ar")
//
//            // Set the view's delegate
//            sceneView.delegate = self
//
//            // Create a new scene
//            let scene = SCNScene(named: "art.scnassets/ship.scn")!
//
//            // Set the scene to the view
//
//            sceneView.scene = scene
//        }
        
        if qr == true
        {
            super.viewDidLoad()
            print("qr")
            //sceneView.delegate = self
            
            // Show statistics such as fps and timing information
            //sceneView.showsStatistics = true
            // Creer la session
            let session = AVCaptureSession()
            
            let captureDevice = AVCaptureDevice.default(for: .video)
            print("qr2")
            do
            {
                print("qr3")
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                session.addInput(input)
            }
            catch{
                print("ERROR")
            }
            print("qr4")
            let output = AVCaptureMetadataOutput()
            session.addOutput(output)
            
            output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            
            output.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
            video = AVCaptureVideoPreviewLayer(session: session)
            video.frame = view.layer.bounds
            view.layer.addSublayer(video)
            print("qr5")
            session.startRunning()
            print("qr6")
        }
        
    }
    
     func loadAR(){
        super.viewDidLoad()
        print("ar")

        // Set the view's delegate
        sceneView.delegate = self
        
        // Create a new scene
        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        
        sceneView.scene = scene
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {return}
        let result = sceneView.hitTest(touch.location(in: sceneView ), types: [ARHitTestResult.ResultType.featurePoint])
        guard let hitResult = result.last else {return}
        let hitTransform = SCNMatrix4(hitResult.worldTransform)
        //let hitTransform = hitResult.worldTransform
        let hitVector = SCNVector3Make(hitTransform.m41, hitTransform.m42, hitTransform.m43)
        
        ar = true
        qr = false
    }
    
    
//        func createBall(position : SCNVector3) {
//            var ballShape = SCNBox(width: 0.01, height: 0.01, length: 0.01, chamferRadius: 0)
//            var ballNode = SCNNode(geometry: ballShape)
//            ballNode.position = position
//            sceneView.scene.rootNode.addChildNode(ballNode)
//        }
        func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            
            if metadataObjects != nil && metadataObjects.count != nil
            {
                if var object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject
                {
                    if object.type == AVMetadataObject.ObjectType.qr
                    {
    //                    print(object.stringValue)
    //                    let alert = UIAlertController(title: "Scan détecté", message: nil, preferredStyle: .alert)
    //
    //                    alert.addAction(UIAlertAction(title: "Voir les sucres", style: .default, handler: nil))
    //                    alert.addAction(UIAlertAction(title: "Nouveau scan", style: .default, handler: nil))
    //                    self.present(alert, animated: true)
                        print("laInfunction")
                        
                        loadAR()
                        
                    }
                }
            }
    



    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
}
